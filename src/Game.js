import React, { Component } from 'react';
import Board from './Board'
import './Game.css';

class Game extends Component {
  render() {
    return (
      <div className="Game">
        <h1 style={{color:'blue'}}>Gomoku</h1>
        <Board />
      </div>
    );
  }
}

export default Game;
