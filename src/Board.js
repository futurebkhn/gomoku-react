import React, { Component } from "react";
import Square from "./Square";

let sizeBoard = 15;
let numberOfSamePoint = 5;
let winner='';

class Board extends Component {
  constructor(props) {
    super(props);
    this.state = {
      squares: Array(sizeBoard)
        .fill()
        .map(x => Array(sizeBoard).fill(null)),
      xIsNext: true,
      xScore: 0,
      yScore: 0
    };
  }

  handleClick = (x, y) => {
    //handle 'X' 'O'
    var squares = [...this.state.squares];
    if (squares[x][y]) return;

    squares[x][y] = this.state.xIsNext ? "X" : "O";
    this.setState({
      squares: squares,
      xIsNext: !this.state.xIsNext
    });

    //check winner

    const isWinner = checkResult(squares, x, y);
    if (isWinner) {
      winner = `Congratulations ! Play ${squares[x][y]} is winner !`;
      var xScore = this.state.xScore;
      var yScore = this.state.yScore;
      if (squares[x][y] === "X") {
        xScore += 1;
        this.setState({
          xScore: xScore
        });
      } else {
        yScore += 1;
        this.setState({
          yScore: yScore
        });
      }
    }

    function nextStep(row, col, srow, scol, times) {
      const nextRow = row + srow * times;
      const nextCol = col + scol * times;
      if (
        sizeBoard <= nextRow ||
        nextRow < 0 ||
        sizeBoard <= nextCol ||
        nextCol < 0
      )
        return null;
      else return { nextRow: nextRow, nextCol: nextCol };
    }
    function checkDirections(squares, row, col, srow, scol) {
      let t;
      const check = [{ row: row, col: col }];
      const currentPointValue = squares[x][y];
      let nextPoint;
      for (t = 1; t < 5; t++) {
        nextPoint = nextStep(row, col, srow, scol, t);
        if (
          !nextPoint ||
          squares[nextPoint.nextRow][nextPoint.nextCol] !== currentPointValue
        ) {
          break;
        }
        check.push(nextPoint);
      }

      for (t = 1; t < 5; t++) {
        nextPoint = nextStep(row, col, -srow, -scol, t);
        if (
          !nextPoint ||
          squares[nextPoint.nextRow][nextPoint.nextCol] !== currentPointValue
        ) {
          break;
        }
        check.push(nextPoint);
      }

      return check;
    }
    function checkResult(squares, row, col) {
      const directions = [
        { stepx: 0, stepy: 1 },
        { stepx: 1, stepy: 1 },
        { stepx: 1, stepy: 0 },
        { stepx: 1, stepy: -1 }
      ];
      for (let i = 0; i < directions.length; i++) {
        const d = directions[i];
        const check = checkDirections(squares, row, col, d.stepx, d.stepy);
        if (check.length === numberOfSamePoint) {
          return true;
        }
      }
      return false;
    }
  };
  handleNewGame() {
    this.setState({
      squares: Array(sizeBoard)
        .fill()
        .map(x => Array(sizeBoard).fill(null)),
      xIsNext: true
    });
    winner=''
  }

  resetScore() {
    this.setState({
      xScore: 0,
      yScore: 0
    });
  }

  render() {
    const style = {
      textAlign: "center",
      margin: "auto",
      height: "auto",
      width: "500px",
      border: "1px solid black",
      tableLayout: "fixed"
    };
    const board = this.state.squares.map((row, x) => {
      return (
        <tr key={"row_" + x}>
          {row.map((col, y) => {
            return (
              <Square
                onClick={() => this.handleClick(x, y)}
                key={x + "_" + y}
                value={this.state.squares[x][y]}
                disabled={winner}
              />
            );
          })}
        </tr>
      );
    });
    return (
      <div>
        <div className="infor-game">
          <div>{winner}</div>
          <div>
            <p>Score X Player : {this.state.xScore}</p>
            <p>Score Y Player : {this.state.yScore}</p>
          </div>
          <button style={{ margin: 20 }} onClick={() => this.handleNewGame()}>
            New Game
          </button>
          <button onClick={() => this.resetScore()}>Reset Score</button>
        </div>
        <div className="table">
        <table cellSpacing="0" style={style}>
          <tbody>{board}</tbody>
        </table>
        </div>
      </div>
    );
  }
}

export default Board;
