import React, { Component } from "react";

class Square extends Component {
  render() {
    return (
      <td
        onClick={this.props.onClick}
        style={{
          overflow: "hidden",
          width: "auto",
          height: "30px",
          border: ".1px solid black"
        }}
      >
        <button
          style={{
            height: 30,
            width:30,
            fontSize:23,
            fontWeight:"bold",
            color: this.props.value ==="X"? 'blue' : 'crimson',
            border:"none"
          }}
          disabled={this.props.disabled}
        >
        {this.props.value}
        </button>
      </td>
    );
  }
}

export default Square;
